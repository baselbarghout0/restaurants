exports.convertCsvToArrayOfObjectsmodule = (csv) =>{
        // split on enter "\n" to separate restaurants 
        let restaurantsInArray = csv.split('\n')
        restaurantsInArray.pop();
        let result = [];
        restaurantsInArray.forEach(element => {
          let restaurant = {}
          //specify days for each restaurant
          restaurant['open_days'] = {
            Sat:null,
            Sun:null,
            Mon:null,
            Tue:null,
            Wed:null,
            Thu:null,
            Fri:null
          }
          // split on , to get each restaurant with its open dates 
          let tmp = element.split(/,(.+)/);
          //get restaurant name
          restaurant['restaurant_name'] = (tmp[0] == undefined) ? tmp[0] : tmp[0].replace(/"/g, "")
          //get restaurant open dates
          let dates = (tmp[1] == undefined) ? tmp[1] : tmp[1].replace(/"/g, "")
          // two kinds of dates, pick the one without "/" separator 
          if(dates != undefined && !dates.includes('/')){
            //split on first number to get the days and times
            let temp = dates.split(/([0-9])/g)
            //get days
            let days = temp[0];
            //get hours
            let hours = temp.join().replace(days,'').replace(/,/g, "").replace(/ /g, "")
            //get days without "," separator
            if(!days.includes(',')){
              //split on "-" 
              let manyDaysWithSameTime = days.split('-')
              // map over them and put the time with its day
              manyDaysWithSameTime.map(el=>{
                restaurant['open_days'][el.trim()] =  hours
                
    
              })
            }
            //get days with "," and "-" separators
            else
            {
              //split on two sperators "," and "-"
              let manyDaysWithSameTime = days.split(/[,-]/)
              manyDaysWithSameTime.map(el=>{
                restaurant['open_days'][el.trim()] =  hours
              })
            }
          }
          //pick the dates with "/" separator 
          if(dates != undefined && dates.includes('/')){
            //get the double dates
            let doubleDates = dates.split('/');
            doubleDates.map(el=>{
              //split on first number 
              let temp = el.split(/([0-9])/g)
              //get the days
              let days = temp[0].trim();
              //get hours
              let hours = temp.join().replace(days,'').replace(/,/g, "").replace(/ /g, "")
              //check days without "," separtor
              if(!days.includes(',')){
                //get days 
                temp = days.split('-')
                //get hours per day
                temp.map(el=>{
                  restaurant['open_days'][el.trim()] =  hours
                })
              }
              //check days without "," and "-" separtor
    
              else
              {
                let manyDays = days.split(/[,-]/)
                manyDays.map(el=>{
                  restaurant['open_days'][el.trim()] =  hours
    
                })
              }
            })
         }
         result.push(restaurant)
        });
    return result;
  }
  
  exports.checkOpenResturants = (restaurants,day )=>{
    //pick the first three letters from the day
      if(day.length > 3){
          day = day.substring(0,3)
      }
      //formate the day
      day =  day.toLowerCase()
      day = day.charAt(0).toUpperCase() + day.slice(1)
      //get the opening days
      let openResturants = restaurants.filter(el=>el.open_days[day]!=null)
      //remove the closed days
      for(let item in openResturants){
        let days = openResturants[item]['open_days']
        for(let day in days){
          if(days[day] == null) delete days[day]
        }
      }
      return openResturants
     }
