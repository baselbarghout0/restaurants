const express = require("express");
const app = express();
const {getRestaurantData} = require("./restaurantsData");
const uti = require('./utilities')
app.get("/:date", (req, res) => {
  getRestaurantData().then((data) => {
      let formatedData = uti.convertCsvToArrayOfObjectsmodule(data)
      let answer = uti.checkOpenResturants(formatedData,req.params.date)
    res.send(answer);
  });
});

app.listen(3002, () => console.log("Server is working"));