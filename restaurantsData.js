const https = require("https");

const getRestaurantData = () => {
  return new Promise(function (resolve, reject) {
    let data = "";
    https
      .get(
        "https://gist.githubusercontent.com/rodbegbie/9321897/raw/0468834c3044e5e4e6c2779bfca377d9f01d3abe/rest_hours.csv",
        (resp) => {
          resp.on("data", (chunk) => {
            data += chunk;
          });
          resp.on("end", () => {
            resolve(data);
          });
        }
      )
      .on("error", (err) => {
        reject(err.message);
      });
  });
};

exports.getRestaurantData = getRestaurantData;